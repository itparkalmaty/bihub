<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function business_type()
    {
        return $this->belongsTo(Business_type::class);
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }
}
