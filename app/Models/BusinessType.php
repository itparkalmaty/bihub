<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessType extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
    ];


    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function children()
    {
        return $this->hasMany(BusinessType::class);
    }

    public function businesses($request)
    {
        return $this->business_type_id == null ? $this->parentFiltration($request) : $this->childFiltration($request);
    }

    public function parentFiltration($request)
    {
        return $this->hasManyThrough(Business::class, BusinessType::class)->where(function ($query) use ($request) {
            if ($request->has('search')) {
                if (strlen($request->search) < 3) {
                    $search = '%' . $request->search;
                } else {
                    $search = '%' . $request->search . '%';
                }
                $query->where('title', 'iLIKE', '%' . $search . '%');
            }

            if ($request->has('category_id'))
                $query->where('category_id', $request->category_id);

            if ($request->has('business_type_id'))
                $query->where('business_type_id', $request->business_type_id);

            if ($request->has('status_id'))
                $query->where('status_id', $request->status_id);

            if ($request->has('city_id'))
                $query->where('city_id', $request->city_id);

            if ($request->has('tags')){
                $query->whereHas('tags', function ($childQuery) use ($request){
                    $childQuery->whereIn('id', $request->tags);
                });
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('min_price', '>=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('min_price', '<=', $request->price_to);
            }

        })->orderBy('id', 'desc')->paginate(12);
    }

    public function childFiltration($request)
    {
        return $this->hasMany(Business::class)->where(function ($query) use ($request) {
            if ($request->has('search')) {
                if (strlen($request->search) < 3) {
                    $search = '%' . $request->search;
                } else {
                    $search = '%' . $request->search . '%';
                }
                $query->where('title', 'iLIKE', '%' . $search . '%');
            }

            if ($request->has('category_id'))
                $query->where('category_id', $request->category_id);

            if ($request->has('business_type_id'))
                $query->where('business_type_id', $request->business_type_id);

            if ($request->has('status_id'))
                $query->where('status_id', $request->status_id);

            if ($request->has('city_id'))
                $query->where('city_id', $request->city_id);

            if ($request->has('tags')){
                $query->whereHas('tags', function ($childQuery) use ($request){
                    $childQuery->whereIn('id', $request->tags);
                });
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('min_price', '>=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('min_price', '<=', $request->price_to);
            }
        })->orderBy('id', 'desc')->paginate(12);
    }

    public function business_type()
    {
        return $this->belongsTo(Business_type::class);
    }
}

