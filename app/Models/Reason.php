<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reason extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'user_id',
        'body',
        'reasonable_id',
        'reasonable_type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reasonable()
    {
        return $this->morphTo();
    }
}
