<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'main_image',
        'name',
        'content',
        'order',
        'citation',
        'images',
        'slug_name',
        'article_category_id'
    ];

    public function getRouteKeyName()
    {
        return 'slug_name';
    }

    public function article_category()
    {
        return $this->belongsTo(Article_category::class);
    }

}
