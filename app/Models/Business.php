<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'slug',
        'title',
        'subtitle',
        'image',
        'min_price',
        'idea',
        'state',
        'details',
        'videos',
        'city_id',
        'currency_id',
        'user_id',
        'status_id',
        'category_id',
        'business_type_id',
        'images',
        'phone',
        'document',
        'percentage',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function business_type()
    {
        return $this->belongsTo(Business_type::class);
    }
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function reasons()
    {
        return $this->morphMany(Reason::class,'reasonable');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'tags_businesses');
    }
}
