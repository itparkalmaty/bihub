<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Freelancer extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'description',
        'verification_image',
        'avatar',
        'price',
        'status_id',
        'currency_id',
        'user_id',
        'nickname',
        'phone'
    ];

    public function getRouteKeyName()
    {
        return 'nickname';
    }

    public function categories()
    {
        return $this->belongsToMany(FreelanceCategory::class,'freelancer_freelance_category');
    }

    public function reasons()
    {
        return $this->morphMany(Reason::class,'reasonable');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_freelancers');
    }
}
