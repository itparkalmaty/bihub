<?php

namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;

class BusinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|max:255|unique:businesses',
            'subtitle' => 'required|max:255',
            'images' => 'nullable',
            'min_price' => 'required|numeric',
            'idea' => 'nullable',
            'state' => 'nullable',
            'details' => 'nullable',
            'videos' => 'nullable',
            'document' => 'nullable',
            'city_id' => 'required|exists:cities,id',
            'currency_id' => 'required|exists:currencies,id',
            'category_id' => 'required|exists:categories,id',
            'business_type_id' => 'required|exists:business_types,id',
            'phone' => 'required',
            'percentage' => 'nullable'
        ];
    }
}
