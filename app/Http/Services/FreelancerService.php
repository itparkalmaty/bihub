<?php


namespace App\Http\Services;


class FreelancerService
{

    public function setDefaultValues($data)
    {
        if ($data['verification_image'] != '')
            $data['verification_image'] = str_replace('public/','storage/',$data['verification_image']->store('public/freelancers/verifciations'));


        if ($data['avatar'] != '')
            $data['avatar'] = str_replace('public/','storage/',$data['avatar']->store('public/freelancers/avatar'));

        $data['status_id'] = 1;

        return $data;

    }

    public function setDefaultValueUpdate($data)
    {
        if ($data['avatar'] != ''){
            $data['avatar'] = str_replace('public/','storage/',$data['avatar']->store('public/freelancers/avatar'));
        }else{
            unset($data['avatar']);
        }

        $data['status_id'] = 1;

        return $data;

    }
}
