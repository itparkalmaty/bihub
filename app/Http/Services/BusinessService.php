<?php


namespace App\Http\Services;



use Illuminate\Support\Str;

class BusinessService
{

    public function saveImage($images)
    {
        $imagesLinks = [];

        foreach ($images as $image){
            $imagesLinks[] = str_replace('public/','storage/',$image->store('public/businesses/images'));
        }
        return json_encode($imagesLinks);
    }

    public function handleDocument($document)
    {
        return str_replace('public/','storage/',$document->store('public/businesses/document'));
    }

    public function handleVideos($videos)
    {
        $videoArray = [];

        foreach ($videos as $video){
            $videoArray[] = $video->store('/businesses/videos');

        }

        return $videoArray;
    }

    public function setDefaultValues($data,$user)
    {

        $data['slug'] = Str::slug($data['title'],'-');

        $data['user_id'] = $user->id;

        $data['status_id'] = 1;

        return $data;
    }
}
