<?php


namespace App\Http\Services;


use App\Models\Business;
use App\Models\User;
use App\Models\Viewing;

class UserService
{
    public function addViews(Business $business,$user)
    {
        $view = Viewing::where('business_id', $business->id)->where('user_id', $user)->first();
        if ($view)
            $view->delete();

        Viewing::create([
            'business_id' => $business->id,
            'user_id' => $user
        ]);


    }
}
