<?php


namespace App\Http\Services;


use App\Models\SmsCode;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class SmsService
{
    protected $token;
    public function send(SmsCode $smsCode, User $user)
    {
        $sms_code_array = [
            'login' => 'IRONL',
            'psw' => 'Timatima02tima',
            'phones' => $user['phone'],
            'mes' => "Ваш код $smsCode->code, BHUB",
        ];

//        $data = http_build_query([
//            'receiver' => $user->phone,
//            'text' => "Ваш код $smsCode->code, BHUB",
//            'apiKey' => $this->token
//        ]);

        $response = Http::get('https://smsc.ru/sys/send.php?login='.$sms_code_array['login'].'&psw='.$sms_code_array['psw'].'&phones='.$sms_code_array['phones'].'&mes='.$sms_code_array['mes']);
        if ($response->successful()) {
            return true;
        } else {
            return false;
        }
    }

    public function generate(User $user)
    {
        if ($user->code) {
            $user->code->update([
                'code' => mt_rand(1000, 9999)
            ]);
        } else {
            $code = SmsCode::create([
                'user_id' => $user->id,
                'code' => mt_rand(1000, 9999)
            ]);
        }
        return $this->send($code, $user);
    }
}
