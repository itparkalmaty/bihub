<?php

namespace App\Http\Controllers\Api\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Main\BusinessRequest;
use App\Http\Resources\BusinessResource;
use App\Http\Resources\BusinessTypeResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\TagResource;
use App\Http\Services\BusinessService;
use App\Http\Services\UserService;
use App\Models\Business;
use App\Models\BusinessType;
use App\Models\Category;
use App\Models\City;
use App\Models\Currency;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BusinessController extends Controller
{
    //
    protected $service;

    protected $user_service;

    public function __construct()
    {
        $this->service = new BusinessService();
        $this->user_service = new UserService();
    }

    public function types(BusinessType $businessType)
    {
        if ($businessType->name)
            return BusinessTypeResource::collection($businessType->children);

        return BusinessTypeResource::collection(BusinessType::get());

    }

    public function cities()
    {
        return CityResource::collection(City::get());
    }

    public function categories(BusinessType $businessType)
    {
        return CategoryResource::collection($businessType->categories);
    }

    public function index(Request $request, BusinessType $businessType)
    {
        return BusinessResource::collection($businessType->businesses($request));
    }

    public function tags()
    {
        return TagResource::collection(Tag::get());
    }


    public function store(BusinessRequest $request)
    {
        $data = $request->all();


        if ($request->hasFile('document')) {
            $data['document'] = $this->service->handleDocument($request->file('document'));
        }


        $data = $this->service->setDefaultValues($data, $request->user());

//        return  response($request->images);
        if ($request->has('images'))
            $data['images'] = $this->service->saveImage($request->images);


        $business = Business::create($data);

        return response(['business' => $business], 201);
    }

    public function  show(Business $business, Request $request)
    {
//        dump($request->user());
        if ($request->has('user_id'))
            $this->user_service->addViews($business, $request->user_id);

        return new BusinessResource($business);
    }

    public function currencies()
    {
        return response(['currency' => Currency::get()]);
    }

    public function categoriesMain(Request $request)
    {
        return CategoryResource::collection(Category::where(function($query) use ($request){
            if($request->has('type'))
                $query->where('business_type_id',$request->type);
        })->get());
    }


}
