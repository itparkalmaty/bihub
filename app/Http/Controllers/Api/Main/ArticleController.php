<?php

namespace App\Http\Controllers\Api\Main;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCategoryResource;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    //
    public function index(Request $request)
    {
        return ArticleResource::collection(Article::where(function ($query) use ($request) {
            if ($request->has('search')) {
                if (strlen($request->search) < 3) {
                    $search = $request->search . '%';
                } else {
                    $search = '%' . $request->search . '%';
                }
                $query->where('name', 'iLIKE', $search);
            }

            if ($request->has('category_id'))
                $query->where('article_category_id',$request->category_id);
        })->paginate(12));
    }


    public function show(Article $article)
    {
        return new ArticleResource($article);
    }

    public function categories()
    {
        return ArticleCategoryResource::collection(ArticleCategory::get());
    }
}
