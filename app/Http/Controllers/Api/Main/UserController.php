<?php

namespace App\Http\Controllers\Api\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\FreelancerStoreRequest;
use App\Http\Requests\FreelancerUpdateRequest;
use App\Http\Resources\BusinessResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\ViewingResource;
use App\Http\Services\FreelancerService;
use App\Models\Freelancer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //

    protected $service;
    public function __construct()
    {
        $this->service = new FreelancerService();
    }
    public function user(Request $request)
    {
        return response(['user' => new UserResource($request->user())],200);

    }


    public function updateProfile(Request $request)
    {
        $data = $request->all();
        if($request->has('password') && $request->password != ''){
            $data['password'] = Hash::make($request->password);
        }else{
            unset($data['password']);
        }

        $user = $request->user()->update($data);

        return response(['user' => $user],200);

    }

    public function views(Request $request)
    {
        return ViewingResource::collection($request->user()->views);
    }


    public function businesses(Request $request)
    {
        return BusinessResource::collection($request->user()->businesses->where('business_type_id',$request->business_type_id));
    }

    public function updateFreelancerProfile(FreelancerUpdateRequest $request)
    {
        $data = $request->except('verification_image');


        $data = $this->service->setDefaultValueUpdate($data);

        $freelancer = $request->user()->freelancer->update($data);

        return response(['freelancer' => $freelancer], 200);


    }
}
