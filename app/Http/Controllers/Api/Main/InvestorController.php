<?php

namespace App\Http\Controllers\Api\Main;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvestorResource;
use App\Models\Investor;
use Illuminate\Http\Request;

class InvestorController extends Controller
{
    //
    public function index()
    {
        return InvestorResource::collection(Investor::where('is_active',1)->orderBy('id','asc')->get());
    }
}
