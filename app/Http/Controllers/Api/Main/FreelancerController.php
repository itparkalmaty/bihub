<?php

namespace App\Http\Controllers\Api\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\FreelancerIndexRequest;
use App\Http\Requests\FreelancerStoreRequest;
use App\Http\Resources\FreelancerCategoryResource;
use App\Http\Resources\FreelancerResource;
use App\Http\Services\FreelancerService;
use App\Models\FreelanceCategory;
use App\Models\Freelancer;
use Illuminate\Http\Request;

class FreelancerController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new FreelancerService();
    }

    //
    public function index(FreelancerIndexRequest $request)
    {
        $freelancers = Freelancer::orderBy('id', 'desc')->whereHas('user', function ($query) use ($request) {
            if ($request->has('search')) {
                if (strlen($request->search) < 3) {
                    $search = $request->search . '%';
                } else {
                    $search = '%' . $request->search . '%';
                }
                $query->where('name', 'iLIKE', $search);
            }

        })
//            ->whereHas('categories', function ($query) use ($request) {
//                if ($request->has('category_id'))
//                    $query->where('id', $request->category_id);
//            })
            ->paginate(10);
        return response(['freelancers' => FreelancerResource::collection($freelancers)], 200);
    }

    public function getCategory()
    {
        return FreelancerCategoryResource::collection(FreelanceCategory::get());
    }


    public function show(Freelancer $freelancer)
    {
        return new FreelancerResource($freelancer);
    }

    public function store(FreelancerStoreRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $data = $this->service->setDefaultValues($data);

        $freelancer = Freelancer::create($data);

        return response(['freelancer' => $freelancer], 201);
    }

    public function addCategory(FreelanceCategory $freelanceCategory, Request $request)
    {
        $user = $request->user();
        if ($user->freelancer) {
            if (!$user->freelancer->categories->contains($freelanceCategory)) {
                $user->freelancer->categories->attach($freelanceCategory);

                return response(['categories' => $user->freelancer->categories, 'message' => 'Категория успешно добавлена'], 201);
            } else {
                $user->freelancer->categories->detach($freelanceCategory);

                return response(['categories' => $user->freelancer->categories, 'message' => 'Категория успешно убрана'], 200);

            }
        }
        return response(['message' => 'У вас нету аккаунта фрилансера'], 422);
    }
}
