<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class   LoginController extends Controller
{
    //
    public function login(LoginRequest $request)
    {
        $user = User::where('email',$request->email)->first();

        if (Hash::check($request->password,$user->password)){
            $token = $user->createToken(env('APP_NAME'));
            return response([
                'user' => $user,
                'token' => $token->plainTextToken,
                'message' => 'Вы успешно вошли в систему',

            ],200);
        }else{
            return response([
                'message' => 'У вас неправильный логин или пароль'
            ],422);
        }
    }
}
