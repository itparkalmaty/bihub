<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterCheckRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Http\Services\SmsService;
use App\Models\SmsCode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    protected $sms_service;

    public function __construct()
    {
        $this->sms_service = new SmsService();
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);
        $this->sms_service->generate($user);

        $token = $user->createToken(env('APP_NAME'));

        return response([
            'user' => $user,
            'message' => 'На Ваш номер было отправлено 4-х значный код',
            'token' => $token->plainTextToken
        ], 201);
    }

    public function check(RegisterCheckRequest $request, User $user)
    {
        if ($user->code->code == $request->code) {
            $token = $user->createToken(env('APP_NAME'));
            return response()->json([
                'message' => 'Вы успешно зашли в систему',
                'token' => $token->plainTextToken
            ],201);
        } else {
            return response(['message' => 'Неверный код']);
        }
    }
}
