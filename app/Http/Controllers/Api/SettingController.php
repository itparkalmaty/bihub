<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Backpack\Settings\app\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    //

    public function index()
    {
        $settings = Setting::all();
        $mainSettings = [];
        foreach ($settings as $setting){
            $mainSettings[$setting['key']] = $setting['value'];
        }

        return response(['settings' => $mainSettings],200);
    }
}
