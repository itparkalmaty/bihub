<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Article::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/article');
        CRUD::setEntityNameStrings('новость', 'новости');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'main_image', // The db column name
            'label'     => 'Основное фото', // Table column heading
            'type'      => 'image',
        ]);
        $this->crud->addColumn([
            'name'      => 'name', // The db column name
            'label'     => 'Название', // Table column heading
            'type'      => 'text',
        ]);
        $this->crud->addColumn([
            'name'      => 'citation', // The db column name
            'label'     => 'Цитирование', // Table column heading
            'type'      => 'text',
        ]);

        CRUD::column('slug_name')->label('Слаг');
        CRUD::column('article_category_id')->label('Категория новостей');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ArticleRequest::class);

        $this->crud->addField([
            'name'  => 'main_image',
            'label' => 'Главное фото',
            'type'  => 'browse'
        ]);
        $this->crud->addField([
            'name'  => 'images',
            'label' => 'Фотки',
            'type'  => 'browse_multiple'
        ]);
        CRUD::field('name')->label('Название');
        CRUD::field('content')->label('Контент');
        CRUD::field('order')->label('Порядок');
        CRUD::field('citation')->label('Цитирование');
        CRUD::field('slug_name')->label('Слаг');
        CRUD::field('article_category_id')->label('Категория новостей');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
