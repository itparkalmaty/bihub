<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BusinessRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BusinessCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BusinessCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Business::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/business');
        CRUD::setEntityNameStrings('Бизнес - Стартап', 'Бизнесы - Стартапы');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('title')->label('Название');
        CRUD::column('subtitle')->label('Подтекст');
        $this->crud->addColumn([
            'name' => 'avatar', // The db column name
            'label' => 'Фото', // Table column heading
            'type' => 'image',
        ]);
        CRUD::column('min_price')->label('Цена');
        CRUD::column('city_id')->label('Город');
        CRUD::column('currency_id')->label('Валюта');
        CRUD::column('user_id')->label('Пользователь');
        CRUD::column('status_id')->label('Статус');
        CRUD::column('category_id');
        CRUD::column('business_type_id');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BusinessRequest::class);

        CRUD::field('slug');
        CRUD::field('title');
        CRUD::field('subtitle');
        CRUD::field('min_price');
        CRUD::field('state');
        CRUD::field('city_id');
        CRUD::field('currency_id');
        CRUD::field('videos');
        CRUD::field('user_id');
        CRUD::field('status_id');
        CRUD::field('business_type_id');
        CRUD::field('category_id');
        $this->crud->addField([
            'type' => 'select2_multiple',
            'name' => 'tags',
            'entity' => 'tags',
            'attribute' => 'title',
            'pivot' => true,
        ]);
        $this->crud->addField([
            'name' => 'idea',
            'label' => 'Идея',
            'type' => 'ckeditor',

            // optional:
            'options' => [
                'autoGrow_minHeight' => 200,
            ]
        ]);
        $this->crud->addField([
            'name' => 'details',
            'label' => 'Детали',
            'type' => 'ckeditor',

            // optional:
            'options' => [
                'autoGrow_minHeight' => 200,
            ]
        ]);
        $this->crud->addField([
            'name' => 'images',
            'label' => 'Фотки',
            'type' => 'browse_multiple',
            'multiple' => true
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
