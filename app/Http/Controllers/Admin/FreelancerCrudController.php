<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FreelancerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FreelancerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FreelancerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Freelancer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/freelancer');
        CRUD::setEntityNameStrings('фрилансер', 'фрилансеры');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('description')->label('Описание');
        $this->crud->addColumn([
            'name'      => 'verification_image', // The db column name
            'label'     => 'Фото верификации', // Table column heading
            'type'      => 'image',
        ]);

        $this->crud->addColumn([
            'name'      => 'avatar', // The db column name
            'label'     => 'Аватар', // Table column heading
            'type'      => 'image',
        ]);
        CRUD::column('price')->label('Цена');
        CRUD::column('status_id')->label('Статус');
        CRUD::column('currency_id')->label('Валюта');
        CRUD::column('user_id')->label('Пользователь');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FreelancerRequest::class);

        CRUD::field('description');
        $this->crud->addField([
            'name'          => 'avatar',
            'label'         => 'Фотки',
            'type'          => 'browse',
            'multiple'      => true
        ]);
        $this->crud->addField([
            'name'          => 'verification_image',
            'label'         => 'Фотки',
            'type'          => 'browse',
            'multiple'      => true
        ]);
        CRUD::field('nickname')->label('Никнейм');
        CRUD::field('price')->label('Цена');
        CRUD::field('status_id')->label('Статус');
        CRUD::field('currency_id')->label('Валюта');
        CRUD::field('user_id')->label('Пользователь');
        $this->crud->addField([
            'type' => 'select2_multiple',
            'name' => 'tags',
            'entity' => 'tags',
            'attribute' => 'title',
            'pivot' => true,
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
