<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ViewingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'business' => new BusinessResource($this->business),
            'id' => $this->id,
            'created_at' => $this->created_at->format('d.m.y')
        ];
    }
}
