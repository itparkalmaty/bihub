<?php

namespace App\Http\Resources;

use App\Traits\ImageTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class   BusinessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'idea' => $this->idea,
            'state' => $this->state,
            'details' => $this->details,
            'videos' => $this->videos,
            'document' => asset($this->document),
            'views' => $this->views,
            'updated_at' => $this->updated_at->format('d.m.y'),
            'slug' => $this->slug,
            'category' => $this->category,
            'user' => $this->user,
            'city' => new CityResource($this->city),
            'currency' => $this->currency,
            'business_type' => $this->business_type,
            'images' => $this->images ? ImageTrait::encodeImages($this->images) : null,
            'price' => $this->min_price,
            'phone' => $this->phone,
            'percentage' => $this->percentage,
            'tags' => $this->tags
        ];
    }
}
