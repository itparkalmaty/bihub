<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FreelancerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => asset($this->avatar),
            'price' => $this->price,
            'description' => $this->description,
            'categories' => FreelancerCategoryResource::collection($this->categories),
            'user' => $this->user,
            'currency' => $this->currency,
            'status' => $this->status,
            'nickname' => $this->nickname,
            'phone' => $this->phone
        ];
    }
}
