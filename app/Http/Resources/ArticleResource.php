<?php

namespace App\Http\Resources;

use App\Traits\ImageTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'main_image' => asset($this->main_image),
            'content' => $this->content,
            'citation' => $this->citation,
            'name' => $this->name,
            'repost_counter' => $this->repost_counter,
            'images' => ImageTrait::encodeImages($this->images),
            'created_at' => $this->created_at->format('d.m.y'),
            'updated_at' => $this->updated_at->format('d.m.y'),
            'slug' => $this->slug_name,
            'article_category' => $this->article_category,
        ];
    }


}
