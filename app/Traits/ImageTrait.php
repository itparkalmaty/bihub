<?php


namespace App\Traits;


class ImageTrait
{
    public static function encodeImages($images)
    {
        $images = json_decode($images);
        if (is_array($images))
            foreach ($images as $key => $image) {
                $images[$key] = asset($image);
            }
        return $images;
    }
}
