<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('settings')->insert([
            'key' => 'banner_title',
            'name' => 'Баннера текст',
            'description' => 'Введите текст',
            'value' => 'ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);
        DB::table('settings')->insert([
            'key' => 'investment_text',
            'name' => 'Инвестируйте текст',
            'description' => 'Введите текст',
            'value' => 'Добавьте Ваш Инвестиционный Интерес и получайте обращения от проектов',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);
        DB::table('settings')->insert([
            'key' => 'find_investment_text',
            'name' => 'Найти инвестиции текст',
            'description' => 'Введите текст',
            'value' => 'Помогаем начинающим и опытным предпринимателям найти финансирование',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);


        DB::table('settings')->insert([
            'key' => 'for_investment_text',
            'name' => 'Описание для инвесторов и работодателей',
            'description' => 'Введите текст',
            'value' => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'strategy_text',
            'name' => 'Текст стратегий',
            'description' => 'Введите текст',
            'value' => 'Начинающему оратору отточить навык публичных выступлений в домашних условиях.',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'analytic_text',
            'name' => 'Текст аналитики',
            'description' => 'Введите текст',
            'value' => 'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'experience_text',
            'name' => 'Текст опыта',
            'description' => 'Введите текст',
            'value' => 'Несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'startup_text',
            'name' => 'Текст стартапа',
            'description' => 'Введите текст',
            'value' => 'В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит.',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'start_text',
            'name' => 'Текст начать',
            'description' => 'Введите текст',
            'value' => 'При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'startups_count_text',
            'name' => 'Текст внизу количества стартапов',
            'description' => 'Введите текст',
            'value' => 'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'businesses_sale_text',
            'name' => 'Продажи текст',
            'description' => 'Введите текст',
            'value' => 'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'investment_count_text',
            'name' => 'Инвестиционные проекты текст',
            'description' => 'Введите текст',
            'value' => 'Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1

        ]);

        DB::table('settings')->insert([
            'key' => 'socials_text',
            'name' => 'Мы в соц сетях текст',
            'description' => 'Введите текст',
            'value' => 'В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.',
            'field' => '{"name":"value","label":"Value","type":"text"}',
            'active' => 1
        ]);



    }
}
