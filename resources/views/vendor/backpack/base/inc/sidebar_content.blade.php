<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}">{{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'> Категории</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('currency') }}'> Валюты</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('status') }}'> Статусы</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('city') }}'> Города</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'> Страны</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('business') }}'> Бизнесы - Стартапы</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('article_category') }}'> Категория новостей</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('business_type') }}'> Тип бизнесов</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('freelance_category') }}'> Категория фрилансеров</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('freelancer') }}'> Фрилансеры</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'> Новости</a></li>

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'> Пользователи</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'> <span>Настройки</span></a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('question') }}'>FAQ</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('investor') }}'>Инвесторы</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-question'></i> Tags</a></li>