<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('currency', 'CurrencyCrudController');
    Route::crud('status', 'StatusCrudController');
    Route::crud('city', 'CityCrudController');
    Route::crud('country', 'CountryCrudController');
    Route::crud('business', 'BusinessCrudController');
    Route::crud('article_category', 'Article_categoryCrudController');
    Route::crud('business_type', 'Business_typeCrudController');
    Route::crud('freelancer_category', 'Freelancer_categoryCrudController');
    Route::crud('freelance_category', 'Freelance_categoryCrudController');
    Route::crud('freelancer', 'FreelancerCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('reason', 'ReasonCrudController');
    Route::crud('question', 'QuestionCrudController');
    Route::crud('investor', 'InvestorCrudController');
    Route::crud('tag', 'TagCrudController');
}); // this should be the absolute last line of this file