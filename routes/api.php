<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Main\ArticleController;
use App\Http\Controllers\Api\Main\BusinessController;
use App\Http\Controllers\Api\Main\FreelancerController;
use App\Http\Controllers\Api\Main\InvestorController;
use App\Http\Controllers\Api\Main\QuestionController;
use App\Http\Controllers\Api\Main\UserController;
use App\Http\Controllers\Api\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::prefix('/auth')->group(function(){
    Route::post('/login',[LoginController::class,'login']);
    Route::post('/register',[RegisterController::class,'register']);
    Route::post('/check/{user}',[RegisterController::class, 'check']);
});

Route::prefix('/user')->middleware('auth:sanctum')->group(function(){
    Route::get('/',[UserController::class,'user']);

    Route::post('/',[UserController::class,'updateProfile']);
    Route::get('/views',[UserController::class,'views']);
    Route::get('/businesses',[UserController::class,'businesses']);

    Route::post('/freelancer',[UserController::class,'updateFreelancerProfile']);
});

Route::prefix('/article')->group(function(){
    Route::get('/',[ArticleController::class,'index'])->name('article.index');
    Route::get('/categories',[ArticleController::class,'categories']);
    Route::get('/{article}',[ArticleController::class,'show']);
});


Route::resource('/faq',QuestionController::class);
Route::resource('/investors',InvestorController::class);

Route::get('/settings',[SettingController::class,'index']);
Route::prefix('/businesses')->group(function(){
    Route::get('/types/{businessType?}',[BusinessController::class,'types']);
    Route::get('/categories/all',[BusinessController::class,'categoriesMain']);

    Route::get('/categories/{businessType}',[BusinessController::class,'categories']);
    Route::get('/show/{business}',[BusinessController::class,'show']);
    Route::get('/currencies',[BusinessController::class,'currencies']);
    Route::get('/cities',[BusinessController::class,'cities']);
    Route::get('/tags', [BusinessController::class, 'tags']);
    Route::get('/{businessType}',[BusinessController::class,'index'])->name('business.index');

    Route::post('/',[BusinessController::class,'store'])->middleware('auth:sanctum');
});

Route::prefix('/freelancers')->group(function(){
    Route::get('/categories',[FreelancerController::class,'getCategory']);
    Route::get('/',[FreelancerController::class,'index'])->name('freelancers.index');
    Route::post('/',[FreelancerController::class,'store'])->middleware('auth:sanctum');
    Route::get('/{freelancer}',[FreelancerController::class,'show']);
    Route::post('/{freelanceCategory}',[FreelancerController::class,'addCategory']);
});

