<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginWithNotCorrectPassword extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/auth/login',[
            'email' => 'tomboffos@gmail.com',
            'password' => '11072000'
        ]);

        $response->assertJson(['message' => true])
            ->assertStatus(422);
    }
}
