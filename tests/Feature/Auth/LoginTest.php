<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/auth/login',[
            'email' => 'tombofos@gmail.com',
            'password' => '11072000a'
        ]);

        $response->assertJson([
            'token' => true,
            'user' => true,
            'message' => true
        ])->assertStatus(200);
    }
}
